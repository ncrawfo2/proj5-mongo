# Project 5: Add Brevet times to MongoDB

Reimplement the RUSA ACP controle time calculator with flask and ajax and populate a database that can be accessed later.

Credits to Michal Young for the initial version of this code.

## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The control[e] times are calculated by dividing the distance of the control location (in kilometers) by the speed in kilometers per hour. The opening times are calculated with the maximum speed and the closing times are calculated with the minimum speed. Depending on the control location the speeds are different. From start to 200 km, the min speed is 15 km/hr and the max speed is 34 km/hr. From 200 - 400 km, the min speed is 15 km/hr and the max speed is 32 km/hr. From 400 - 600 km, the min speed is 15 km/hr and the max speed is 30 km/hr. From 600 - 1000 km, the min speed is 11.428 km/hr and the max speed is 28 km/hr. The calculator converts all inputs expressed in units of miles to kilometers and rounds the result to the nearest kilometer before being used in calculations. Times are rounded to the nearest minute. The control location distances cannot be less than 0, and cannot be larger than 20% of the end of the brevet. By rule, the closing time for the starting point control is one hour after the official start, this can cause controls that are close by to close before the starting point does. To avoid this the French use an algorithm that sets the maximum time for a contol within 60 km to 20 km/hr plus 1 hour. So, the start is 0/20 + 1hr = 1hr, a control at 20km would have 20/20 + 1hr = 2hr, and a control at 60km would have 60/20 + 1hr = 4hr, which matches the regular algorithm. Beyond 60km the algorithm is standard. There are a few examples at (https://rusa.org/pages/acp-brevet-control-times-calculator) that demonstrate how the calculations are done. Additional background information is given at (https://rusa.org/pages/rulesForRiders).  


## Author : Nathan Crawford
## email  : ncrawfo2@cs.uoregon.edu
